import pprint 
import shotgun_api3


# Shotgun access vars
SERVER_PATH = "https://lyquidindustries.shotgunstudio.com"
SCRIPT_NAME = 'pythonAccess'
SCRIPT_KEY = '7a475af4edcf9f80cca953e3d4b9d9b10d69271d135ecaf7e3fa2eb2e0f14923'
ShotgunConnection = shotgun_api3.Shotgun(SERVER_PATH, SCRIPT_NAME, SCRIPT_KEY)
Gravity1ProjectId = 86

# Returns all users for a specific Shotgun project.
def GetAllUsersForProject(ProjectID):
    packagedUsers = []
    entityType = 'HumanUser'
    filters = [['projects', 'in', [{'type': 'Project', 'id': ProjectID}]]]
    fieldsOfInterest = ['id', 'name', 'projects', 'sg_roles']

    users = ShotgunConnection.find(entityType, filters, fieldsOfInterest)

    # Later we may want to filter by user role
    for user in users:
        roles = []
        for role in user['sg_roles']:
            roles.append((role['name'], role['id']))


        packagedUsers.append((user['name'], user['id'], roles)) # Tuple of (name, ID, roles) to be extracted to C++

    return packagedUsers


# Task Templates create a pre-defined list of tasks for an asset, tagged by pipeline-step. Eg Model, Rig, Surface, Texture, Animate. 
def GetAllTaskTemplatesForProject(ProjectID):
    entityType = "TaskTemplate"
    filters = [] 
    fieldsOfInterest = ['code','id','type'] # code == name

    templates = ShotgunConnection.find(entityType, filters, fieldsOfInterest)
    return templates



# Tasks know of their asset, their assignees and their pipeline step
def GetAllTasksForAsset(AssetID):
    packagedTasks = []
    entityType = "Task"
    filters = [['entity', 'is', {'id': AssetID, 'type' : 'Asset'}]]
    fieldsOfInterest =  ['entity', 'task_assignees', 'content', 'step'] # entity == asset

    tasks = ShotgunConnection.find(entityType, filters, fieldsOfInterest) 
    return tasks


# Adds user to the assignees of a task
def AssignTaskToUser(taskID, userID):
    task = GetTask(taskID)
    existingAssignees = task['task_assignees']
    userAlreadyAssigned = False

    for assignee in existingAssignees:
        if assignee['id'] == userID:
            print assignee['name'] + " already assigned"
            userAlreadyAssigned = True

    if not userAlreadyAssigned:
        newAssignees = [{'id' : userID, 'type' : 'HumanUser'}]
        newAssignees.extend(existingAssignees)
        # Update existing field
        ShotgunConnection.update('Task', taskID, {'task_assignees' : newAssignees})


# Creates a pipeline of tasks for an asset. Eg Model, Rig, Surface, Texture, Animate. 
def CreateAssetWithTaskTemplate(AssetName, TaskTemplateID):
    data = {
    'project': {'type':'Project', 'id' : Gravity1ProjectId},
    'code': AssetName,
    'task_template' : { 'id' :  TaskTemplateID, 'type' : 'TaskTemplate'}
    }
    result = ShotgunConnection.create("Asset", data)
    return result





#####################   Other Tests #####################

def CreateTask(assignToUserID, description):
    data = {
    'project': {'type':'Project', 'id' : Gravity1ProjectId},
    'content': 'Model Fix',
    'entity': {'type':'Asset', 'id' : MoonLanderAssetId},
    'sg_description' : description,
    'task_assignees' : [{'type':'HumanUser', 'id' : assignToUserID}],
    'step' : {'type': 'Step', 'id':34}
    
    }
    result = ShotgunConnection.create("Task", data)
    pprint.pprint(result)




def GetAllProjects():
    projectsForCppBridge = []
    projects = ShotgunConnection.find("Project", [], ['name']);
    for project in projects:
        projectsForCppBridge.append((project['name'], project['id'])) # Tuple of (name,ID) 
    return projectsForCppBridge




def GetAllAssetsForProject(ProjectID):
    assetsForCppBridge = []
    assets = ShotgunConnection.find("Asset", [['project', 'is', {'type': 'Project', 'id': ProjectID}]], ['code','id','type'])
    for asset in assets:
        assetsForCppBridge.append({'name': asset['code'], 'id' : asset['id']}) # dict of name, id 
    return assetsForCppBridge


def GetAllTasksForProject(ProjectID):
    tasksForCppBridge = []
    tasks = ShotgunConnection.find("Task", [['project', 'is', {'type': 'Project', 'id': ProjectID}]], ['entity', 'task_assignees', 'content', 'step']) # Entity here is Link - ie Asset
    for task in tasks:
        tasksForCppBridge.append(())
    return tasks




def GetAllPipelineStepsForProject(ProjectID):
    steps = ShotgunConnection.find("Step", [], ['code', 'project'] ) 
    return steps



def GetTask(id):
    task = ShotgunConnection.find_one("Task", [['id', 'is', id ]], ['entity', 'task_assignees', 'content', 'step'])
    return task




